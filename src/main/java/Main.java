import Frames.first.first;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Stream;

import static javax.imageio.ImageIO.read;

/**
 * @author Seif Eddine Menjli
 * created by Seif Eddine Menjli on 22.02.20
 */

public class Main {

    private static GpioController gpio;
    static GpioPinDigitalOutput q;
    static GpioPinDigitalOutput w;
    static GpioPinDigitalOutput e;


    public static void main(String[] args) throws Exception {
        gpio = GpioFactory.getInstance();
        q = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, PinState.LOW);
        w = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, PinState.LOW);
        e = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, PinState.LOW);


        final first frame = new first();
        frame.setContentPane(new JLabel(new ImageIcon(read(new File("src/main/resources/second.png")))));
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
        frame.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }


            @Override
            public void keyPressed(KeyEvent event) {
                switch (event.getKeyCode()) {
                    case KeyEvent.VK_Q:
                        activatePin(q);
					try {
						frame.setContentPane(new JLabel(new ImageIcon(read(new File("src/main/resources/cover.png")))));
					} catch (IOException e1) {
						e1.printStackTrace();
					}
                        frame.setVisible(true);
                        break;
                    case KeyEvent.VK_W:
                        activatePin(w);
					try {
						frame.setContentPane(new JLabel(new ImageIcon(read(new File("src/main/resources/second.png")))));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
                        frame.setVisible(true);
                        break;
                    case KeyEvent.VK_E:
                        activatePin(e);
					try {
						frame.setContentPane(new JLabel(new ImageIcon(read(new File("src/main/resources/cover.png")))));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
                        frame.setVisible(true);
                        break;
                    default:
                        System.out.println("DEFAULT ....");
                }
            }
        });


    }

    private static void activatePin(GpioPinDigitalOutput gpioPinDigitalOutput) {
        getAllGpio().forEach(e -> {
            if (e == gpioPinDigitalOutput) e.high();
            else e.low();
        });
    }

    public static Stream<GpioPinDigitalOutput> getAllGpio() {
        return Arrays.asList(q, w, e).stream();
    }


}

